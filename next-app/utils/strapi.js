import Strapi from 'strapi-sdk-javascript'
import { STRAPI_API } from '../config'

export default new Strapi(STRAPI_API);
import React from 'react'
import axios from 'axios'
import cookie from 'js-cookie'
import strapi from '../../utils/strapi'
// import { STRAPI_API } from '../../config'

export default class Login extends React.Component {

  handleLogin = async (e) => {
    e.preventDefault()
    const email = 'writer_tung@gmail.com'
    const password = 'happy2code'
    const { jwt, user } = await strapi.login(email, password)
    cookie.set('jwt', jwt)
    cookie.set('username', user.username)
  }

  fetchList = async (e) => {
    e.preventDefault()
    const data = await strapi.getEntries('restaurants')
    console.log(data)
  }
  
  render(){
    return <form onSubmit={this.handleLogin}>
      <button>Login</button>
      <a href="#" onClick={this.fetchList}>Fetch</a>
    </form>
  }
}